import * as moment from 'moment';

class EntriesService {
  
  constructor(options = {}) {
    const defaults = this.defaults();

    this.options = {...defaults, options};

    this.key = 'entries';

    let methods = [
      'setStorageService',
      'defaults',
      'defaultEntry',
      'defaultEntries',
      'get',
      'set',
      'newEntry',
      'updateEntry',
      'validateEntry',
      'months',
      'sort'
    ];

    for (const delta in methods) {
      this[methods[delta]] = this[methods[delta]].bind(this);
    }
  }

  /**
   * Sets the storage service to use.
   * 
   * @param {*} input 
   */
  setStorageService(input) {
    this.storageService = input;
  }

  /**
   * Provides the default options.
   */
  defaults() {
    let output = {
      reset: false,
      showOpeningBalance: true,
      entries: null
    };

    return output;
  }

  /**
   * Returns a default entry object.
   */
  defaultEntry() {
    return {
      new: false,
      auto: false,
      changed: false,
      amount: 0,
      date: '',
      day: '',
      month: '',
      year: '',
      original: {
        day: '',
        amount: 0.
      },
      messages: []
    }
  }

  /**
   * Provides the default entries.
   */
  defaultEntries() {
    return {
      "2018":{
        "entries": {
          1: {
            "entries": [
              {
                "date":"2018/01/12",
                "amount":2000,
                "dateobj":"2018-01-12T07:00:00.000Z",
                "year":"2018",
                "month":"1",
                "day":"12"
              },
              {
                "date":"2018/01/20",
                "amount":2500,
                "dateobj":"2018-01-20T07:00:00.000Z",
                "year":"2018",
                "month":"1",
                "day":"20"
              },
              {
                "date":"2018/01/26",
                "amount":300,
                "dateobj":"2018-01-26T07:00:00.000Z",
                "year":"2018",
                "month":"1",
                "day":"26"
              }
            ]
          },
          "2": {
            "entries": [
              {
                "date":"2018/02/13",
                "amount":1500,
                "dateobj":"2018-02-13T07:00:00.000Z",
                "year":"2018",
                "month":"2",
                "day":"13"
              },
              {
                "date":"2018/02/18",
                "amount":-1000,
                "dateobj":"2018-02-18T07:00:00.000Z",
                "year":"2018",
                "month":"2",
                "day":"18"
              },
              {
                "date":"2018/02/23",
                "amount":900,
                "dateobj":"2018-02-23T07:00:00.000Z",
                "year":"2018",
                "month":"2",
                "day":"23"
              }
            ]
          },
          "3": {
            "entries": [
              {
                "date":"2018/03/05",
                "amount":400,
                "dateobj":"2018-03-05T07:00:00.000Z",
                "year":"2018",
                "month":"3",
                "day":"5"
              }
            ]
          },
          "4": {
            "entries": []
          },
          "5": {
            "entries":[]
          },
          "6": {
            "entries":[]
          },
          "7": {
            "entries":[]
          },
          "8": {
            "entries":[]
          },
          "9": {
            "entries":[]
          },
          "10": {
            "entries":[]
          },
          "11": {
            "entries":[]
          },
          "12": {
            "entries":[]
          }
        }
      }
    };
  }

  /**
   * Responsible for retrieving entries from local storage.
   * 
   * @param {*} reset 
   */
  get(options = {}) {
    options = {...this.options, ...options};

    const {activeYears, reset, showOpeningBalance, entries} = options;

    let output = null;

    if (entries) {
      output = entries;
    } else {
      output = this.storageService.get(this.key);
    }

    if (!output || reset) {
      output = this.defaultEntries();
  
      this.set(output);
    }

    for (const key in activeYears) {
      let year = activeYears[key];

      if (!output[year]) {
        output[year] = {
          entries: {}
        };
      }

      let yearlysummary = output[year];

      let yearlyentries = yearlysummary['entries'];

      yearlysummary.open = null;
      yearlysummary.close = null;

      const months = this.months();

      for (const key in months) {
        const month = parseInt(key, 10) + 1;
        
        if (!yearlyentries[month]) {
          yearlyentries[month] = {
            entries: []
          }
        }

        let monthlysummary = yearlyentries[month];
        let monthlyentries = monthlysummary['entries'];

        monthlysummary.open = null;
        monthlysummary.close = null;

        let found_new_entry = false;

        if (monthlyentries.length) {
          for (const key in monthlyentries) {
            // Ensure each entry has a consistent structure
            let defaults = this.defaultEntry();

            monthlyentries[key] = {...defaults, ...monthlyentries[key]};

            let entry = monthlyentries[key];

            const {day, amount} = entry;

            if (!showOpeningBalance) {
              if (entry.auto) {
                // Remove opening balance entries
                monthlyentries.splice(key, 1);
              }
            }

            if (parseInt(key, 10) === (monthlyentries.length - 1)) {
              if (entry.day === '' && entry.amount === 0) {
                found_new_entry = true;
              }
            }

            entry.changed = false;
            entry.open = null;
            entry.close = null;

            entry.original = {
              day: day,
              amount: amount
            };

            entry.messages = this.validateEntry(entry);
          }

          if (showOpeningBalance && !monthlyentries[0].auto) {
            // Insert an opening balance entry
            let entry_open = this.defaultEntry();

            entry_open.auto = true;
            entry_open.changed = false;
            entry_open.year = year;
            entry_open.month = month;
            entry_open.day = 1;
            entry_open.date = year + '/' + month + '/01';

            entry_open.original = {
              day: entry_open.day,
              amount: entry_open.amount
            }

            monthlyentries.unshift(entry_open);
          }
        }

        if (!found_new_entry) {
          let entry_new = this.defaultEntry();

          entry_new.new = true;
          entry_new.year = year;
          entry_new.month = month;

          monthlyentries.push(entry_new);
        }
      }
    }

    this.sort(output);

    return output;
  }

  /**
   * Responsible for saving entries to local storage.
   * 
   * @param {*} input 
   */
  set(input) {
    let output = {...input};

    for (const year in output) {
      let yearlysummary = output[year];

      for (const month in yearlysummary.entries) {
        let monthlysummary = yearlysummary.entries[month];

        for (const delta in monthlysummary.entries) {
          let entry = monthlysummary.entries[delta];

          entry.changed = false;
          entry.new = false;
        }
      }
    }

    this.sort(output);
    
    this.storageService.set(this.key, output);

    return output;
  }

  /**
   * Responsible for adding a new entry.
   * 
   * @param {*} entries 
   * @param {*} year 
   * @param {*} month 
   * @param {*} delta 
   * @param {*} options 
   */
  newEntry(entries, year, month, delta, options = {}) {
    let entry = this.defaultEntry();

    entry.new = true;
    entry.year = year;
    entry.month = month;

    const start = parseInt(delta, 10) + 1;

    entries[year]['entries'][month]['entries'].splice(start, 0, entry);
  }

  /**
   * Responsible for validating an entry.
   * 
   * @param {*} entry 
   */
  validateEntry(entry) {
    let output = [];

    let {year, month, day, amount} = entry;

    if (day !== '') {
      let input = year + '/' + month + '/' + day;

      let result = moment(input, "YYYY/M/D", true).isValid();

      if (!result) {
        output.push(
          {
            key: 'day',
            value: day,
            message: 'Please specify a valid date.'
          }
        );
      }
    }

    amount = parseInt(amount, 10);

    if (isNaN(amount)) {
      output.push(
        {
          key: 'amount',
          value: amount,
          message: 'Please specify a valid amount.'
        }
      );
    }

    return output;
  }

  /**
   * Responsible for updating an entry.
   * 
   * @param {*} entries 
   * @param {*} change 
   */
  updateEntry(entries, change) {
    let output = {...entries};

    const {year, month, delta, name, value} = change;

    let entry = output[year]['entries'][month]['entries'][delta];

    entry[name] = value;

    if ((entry.day !== entry.original.day) || (entry.amount !== entry.original.amount)) {
      entry.changed = true;
    }
    else {
      entry.changed = false;
    }

    entry.messages = this.validateEntry(entry);

    return output;
  }

  /**
   * Returns an array of months.
   */
  months() {
    return [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
  }

  /**
   * Responsible for sorting entries.
   * 
   * @param {*} entries 
   */
  sort(entries) {
    for (const year in entries) {
      let yearlysummary = entries[year]['entries'];

      const months = this.months();

      for (const key in months) {
        const month = parseInt(key, 10) + 1;

        let monthlysummary = yearlysummary[month]['entries'];

        monthlysummary.sort(
          (a, b) => {
            if (a.auto) {
              // Opening balances should always be displayed first
              return -1;
            } else if (a.day === '' || b.day === '') {
              return -1;
            } else if (parseInt(a.day, 10) <= parseInt(b.day, 10)) {
              return -1;
            } else {
              return 1;
            }
          }
        );
      }
    }
  }

}

export default new EntriesService();