class SettingsService {

  constructor() {
    this.key = 'settings';

    let methods = [
      'setStorageService',
      'defaults',
      'get',
      'set',
    ];

    for (const delta in methods) {
      this[methods[delta]] = this[methods[delta]].bind(this);
    }
  }

  /**
   * Sets the storage service to use.
   * 
   * @param {*} input 
   */
  setStorageService(input) {
    this.storageService = input;
  }

  /**
   * Provides the default settings.
   */
  defaults() {
    const limits = {
      2009: 5000,
      2010: 5000,
      2011: 5000,
      2012: 5000,
      2013: 5500,
      2014: 5500,
      2015: 10000,
      2016: 5500,
      2017: 5500,
      2018: 5500
    };

    const activeYears = Object.keys(limits).map(
      (key) => {
        // This is required in order for the Settings indexOf call to work
        return parseInt(key, 10);
      }
    );

    return {
      limits: limits,
      activeYears: activeYears,
      showOpeningBalance: true,
      showEmpty: true,
      amountFormat: 'cents'
    };
  }

  /**
   * Responsible for retrieving settings from local storage.
   * 
   * @param {*} reset 
   */
  get(reset = false) {
    let output = this.storageService.get(this.key);

    let defaults = this.defaults();

    if (!output || reset) {
      output = defaults;

      this.set(output);
    }
    else {
      // Merge any new settings defined since the last save
      output = {...defaults, ...output};
    }

    return output;
  }

  /**
   * Responsible for saving settings to local storage.
   * 
   * @param {*} input 
   */
  set(input) {
    this.storageService.set(this.key, input);
  }

};

export default new SettingsService();