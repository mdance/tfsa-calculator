class LocalStorageService {

  /**
   * Responsible for retrieving from local storage.
   * 
   * @param {*} reset 
   */
  get(key) {
    let output = localStorage.getItem(key);

    try {
      output = JSON.parse(output);
    } catch (error) {
      output = null;
    }

    return output;
  }

  /**
   * Responsible for writing an item to local storage.
   * 
   * @param {*} key 
   * @param {*} value 
   */
  set(key, value) {
    value = JSON.stringify(value);

    localStorage.setItem(key, value);
  }

}

export default new LocalStorageService();