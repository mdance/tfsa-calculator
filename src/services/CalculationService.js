class CalculationService {

  constructor(options = {}) {
    const defaults = this.defaults();

    this.options = {...defaults, ...options};
  }

  /**
   * Provides the yearly contribution limits.
   */
  limits() {
    return {
      2009: 5000,
      2010: 5000,
      2011: 5000,
      2012: 5000,
      2013: 5500,
      2014: 5500,
      2015: 10000,
      2016: 5500,
      2017: 5500,
      2018: 5500
    };
  }

  /**
   * Provides the default options.
   */
  defaults() {
    let output = {};
    
    const limits = this.limits();

    const activeYears = Object.keys(limits).map(
      (key) => {
        // This is required in order for the Settings indexOf call to work
        return parseInt(key, 10);
      }
    );

    output.limits = limits;
    output.activeYears = activeYears;

    return output;
  }

  /**
   * Responsible for calculating the balance, excess, and interest penalties for entries.
   * 
   * @param {*} entries 
   * @param {*} options 
   */
  calculate(entries, options = {}) {
    const defaults = this.defaults();

    options = {...defaults, ...options};

    const {activeYears, limits} = options;

    let calculation = {
      balance: 0,
      limit: 0,
      excess: 0,
      max: 0,
      penalty: 0
    };

    let max_excess = 0;
    let limit = 0;

    for (const key in activeYears) {
      const year = activeYears[key];

      let yearlysummary = entries[year];
      let penalty_year = 0;
      let max_excess_year = 0;

      yearlysummary.open = {...calculation};

      limit = limits[year];

      calculation.limit += limit;
      calculation.excess -= limit;

      max_excess_year = calculation.excess;

      calculation.max = max_excess_year;

      if (max_excess_year > max_excess) {
        max_excess = max_excess_year;
      }
      
      for (const month in yearlysummary['entries']) {
        let monthlysummary = yearlysummary['entries'][month];

        monthlysummary.open = {...calculation};

        calculation.max = 0;

        for (const key in monthlysummary['entries']) {
          let entry = monthlysummary['entries'][key];

          entry.open = {...calculation};

          calculation.balance += entry.amount;

          calculation.excess = calculation.balance - calculation.limit;

          if (calculation.excess < 0) {
            calculation.excess = 0
          }

          if (calculation.excess > calculation.max) {
            calculation.max = calculation.excess
          }

          entry.close = {...calculation};
        }

        monthlysummary.close = {...calculation};

        if (monthlysummary.close.max) {
          monthlysummary.penalty = monthlysummary.close.max * 0.01;

          penalty_year += monthlysummary.penalty;
        }
      }

      yearlysummary.close = {...calculation};
      yearlysummary.penalty = penalty_year;
    }
  }

}

export default new CalculationService();