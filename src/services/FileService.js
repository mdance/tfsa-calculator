import * as Papa from 'papaparse';
import * as moment from 'moment';
import _ from "lodash";

export default {
  handleFile(options) {
    const config = {
      header: true,
      trimHeader: true,
      encoding: 'utf-8',
      complete: (results, file) => {
        const output = this.processEntries(results, file, options);

        options.success(output);
      },
      error: (error, file) => {
        options.error(error, file);
      }
    };

    Papa.parse(options.file, config);
  },
  processEntries(results, file, options) {
    // todo: add to settings form
    // todo: add csv header toggle to settings form
    const format = 'YYYY/MM/DD';

    const output = {};

    results.data.forEach(
      (entry) => {
        if (entry.hasOwnProperty('Date') && entry.hasOwnProperty('Amount')) {
          const date = entry.Date;
          let amount = parseInt(entry.Amount, 10);

          if (options.amountFormat === 'cents') {
            amount = amount / 100;
          }

          const result = moment(date, format);

          const year  = result.format('YYYY');
          const month = result.format('M');
          const day   = result.format('D');

          if (!output.hasOwnProperty(year)) {
            output[year] = {
              entries: {} 
            }

            _.range(1, 13).forEach(
              (key, value) => {
                output[year]['entries'][key] = {
                  entries: []
                }
              }
            )
          }

          output[year]['entries'][month]['entries'].push(
            {
              date: date,
              amount: amount,
              dateobj: result,
              year: year,
              month: month,
              day: day
            }
          );
        }
      }
    );

    return output;
  }
}