import React, { Component } from "react";

import { FormControl, ButtonToolbar, Button, Glyphicon, Popover } from "react-bootstrap";

import Currency from "../Currency/Currency";

import "./Entry.css";

class Entry extends Component {
  constructor(props) {
    super(props);

    const {day, amount} = props.entry;

    this.state = {
      day: day,
      amount: amount
    };

    let methods = [
      'handleNewEntry',
      'handleEntryChange',
      'handleEntrySave',
      'handleEntryDelete',
    ];

    for (const delta in methods) {
      this[methods[delta]] = this[methods[delta]].bind(this);
    }
  }

  handleNewEntry(event) {
    const change = {
      year: this.props.year,
      month: this.props.month,
      delta: this.props.delta
    };

    this.props.onNewEntry(change);
  }

  handleEntryChange(event) {
    const {target} = event;
    let {name, value} = target;

    this.setState(
      (prevState, props) => {
        let output = {};

        if (name === 'contribution' || name === 'withdrawal') {
          value = Math.abs(value);
    
          if (name === 'withdrawal') {
            value = -value;
          }
    
          name = 'amount';
        }
    
        output[name] = value;

        return output;
      },
      () => {
        const change = {
          year: this.props.year,
          month: this.props.month,
          delta: this.props.delta,
          name: name,
          value: value
        };
  
        this.props.onEntryChange(change);
      }
    );
  }
  
  handleEntrySave(event) {
    const {year, month, delta} = this.props;
    const {day, amount} = this.state;

    const change = {
      year: year,
      month: month,
      delta: delta,
      day: day,
      amount: amount
    };

    this.props.onEntrySave(change);
  }

  handleEntryDelete(event) {
    const change = {
      year: this.props.year,
      month: this.props.month,
      delta: this.props.delta
    };

    this.props.onEntryDelete(change);
  }

  getValidationState() {
    const { changed, messages } = this.props.entry;

    let output = null;

    if (messages.length) {
      output = 'error';
    }
    else if (changed) {
      output = 'warning';
    }

    return output;
  }

  render() {
    const { entry, delta } = this.props;

    let classes = [
      'Entry',
      `Entry-${delta}`
    ];

    let { amount, day, changed, messages } = entry;

    let messages_td = {
      day: [],
      amount: [],
    };

    if (messages.length) {
      messages.forEach(
        (message) => {
          if (message.key === 'day') {
            messages_td.day.push(message.message);
          }
          else {
            messages_td.amount.push(message.message);
          }
        }
      );

      if (messages_td.day.length) {
        messages_td.day = (
          <Popover
            className="popover-entry popover-entry-day"
            placement="bottom"
            positionTop={50}
          >
            {messages_td.day.join(' ')}
          </Popover>
        );
      }

      if (messages_td.amount.length) {
        messages_td.amount = (
          <Popover
            className="popover-entry popover-entry-amount"
            placement="bottom"
            positionTop={50}
            positionLeft={150}
          >
            {messages_td.amount.join(' ')}
          </Popover>
        );
      }
    }

    let contribution = '';
    let withdrawal = '';

    if (amount !== '') {
      if (amount > 0) {
        contribution = amount;
      }
      else if (amount < 0) {
        withdrawal = amount;
      }
    }

    if (entry.new) {
      classes.push('new');
    }

    if (entry.close && entry.close.excess && entry.close.excess > 0) {
      classes.push('excess');
    }

    if (changed) {
      classes.push('changed');
    }

    if (messages.length) {
      classes.push('messages');

      classes.push('message-total-' + messages.length);
    }

    let containerDay = entry.day;
    let containerContribution = contribution;
    let containerWithdrawal = withdrawal;

    if (entry.auto) {
      classes.push('auto');
    }
    else {
      containerDay = [];

      containerDay.push(
        (
          <FormControl
            type="text"
            name="day"
            className="day"
            size="1"
            maxLength="2"
            value={day}
            onChange={this.handleEntryChange}
          />
        ),
        messages_td.day
      );

      containerContribution = [];

      containerContribution.push(
        (
          <FormControl 
            type="number" 
            name="contribution"
            size="2"
            className="amount contribution"
            value={contribution}
            onChange={this.handleEntryChange}
          />
        )
      );

      if (contribution !== '') {
        containerContribution = containerContribution.concat(messages_td.amount);
      }

      containerWithdrawal = [];

      containerWithdrawal.push(
        (
          <FormControl 
            type="number" 
            name="withdrawal"
            size="2"
            className="amount withdrawal"
            value={withdrawal}
            onChange={this.handleEntryChange}
          /> 
        )
      );

      if (withdrawal !== '') {
        containerWithdrawal = containerWithdrawal.concat(messages_td.amount);
      }
    }

    return (
      <tr className={classes.join(' ')}>
        <td className="day">
          {containerDay}
        </td>
        <td className="right">
          {containerContribution}
        </td>
        <td className="right">
          {containerWithdrawal}
        </td>
        <td
          className="amount balance right"
        >
          <Currency
            value={entry.close.balance}
          />
        </td>
        <td
          className="amount limit right"
        >
          <Currency
            value={entry.close.limit}
          />
        </td>
        <td
          className="amount excess right"
        >
          <Currency
            value={entry.close.excess}
          />
        </td>
        <td className="operations">
          <ButtonToolbar>
            <Button 
              className="btn-add"
              onClick={this.handleNewEntry}
            >
              <Glyphicon glyph="plus" />
              <label>
                Add
               </label>
            </Button>
            {
            !entry.auto &&
            <Button 
              className="btn-delete"
              onClick={this.handleEntryDelete}
            >
              <Glyphicon glyph="trash" />
              <label>
                Delete
              </label>
            </Button>
            }
            {
            entry.changed &&
            <Button 
              className="btn-save"
              onClick={this.handleEntrySave}
            >
              <Glyphicon glyph="floppy-disk" />
              <label>
                Save
              </label>
            </Button>
            }
          </ButtonToolbar>
        </td>
      </tr>
    );
  }
}

export default Entry;