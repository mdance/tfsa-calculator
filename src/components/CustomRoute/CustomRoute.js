import React from "react";
import { Route } from "react-router-dom";

import nprogress from 'react-nprogress';
import 'react-nprogress/nprogress.css';

class CustomRoute extends React.Component {
  componentWillMount () {
    nprogress.start()
  }

  componentDidMount () {
    nprogress.done()
  }

  render () {
    const { component: C, props: cProps, ...rest} = this.props;

    return (
      <Route 
        {...rest}
        render={props => <C {...props} {...cProps} />}
      />
    )
  }
}

export default CustomRoute;