import React, { Component } from "react";

import { Table } from "react-bootstrap";

import Entry from "../Entry/Entry";
import Currency from "../Currency/Currency";

import "./MonthEntries.css";

class MonthEntries extends Component {
  render() {
    // TODO: Add drag n drop functionality
    /* eslint-disable no-unused-vars */
    const {settings, year, month, name, entries} = this.props;

    const {showEmpty} = settings;

    let classes = [
      'MonthEntries',
      'MonthEntries-' + month
    ];
    /* eslint-enable no-unused-vars */

    let rows = [];

    let empty = true;

    for (const delta in entries['entries']) {
      let entry = entries['entries'][delta];

      if (!entry.auto && entry.day !== '' && entry.amount !== 0) {
        empty = false;
      }

      rows.push(
        <Entry
          key={year + '_' + month + '_' + delta}
          delta={delta}
          entry={entry}
          year={year}
          month={month}
          onNewEntry={this.props.onNewEntry}
          onEntryChange={this.props.onEntryChange}
          onEntryDelete={this.props.onEntryDelete}
          onEntrySave={this.props.onEntrySave}
        />
      );
    }

    if (!showEmpty && empty) {
      return null;
    }

    let footer = [];

    if (entries['penalty'] && entries['penalty'] !== 0) {
      footer = (
          <tfoot>
            <tr>
              <td colspan="4" />
              <th className="right">
                Max Excess
              </th>
              <td className="amount maxexcess right">
                <Currency
                  value={entries.close.max}
                />
              </td>
            </tr>
            <tr>
              <td colspan="4" />
              <th className="right">
                Penalty
              </th>
              <td className="amount penalty right">
                <Currency
                  value={entries.penalty}
                />
              </td>
            </tr>
          </tfoot>
      );
    }

    return (
      <div className={classes.join(' ')}>
        <h3>
          {name}
        </h3>
        <Table>
          <thead>
            <tr>
              <th>
                Date
              </th>
              <th className="right">
                Contribution
              </th>
              <th className="right">
                Withdrawal
              </th>
              <th className="right">
                Balance
              </th>
              <th className="right">
                Limit
              </th>
              <th className="right">
                Excess
              </th>
              <th>
                Operations
              </th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
          {footer}
        </Table>
      </div>
    );
  }
}

export default MonthEntries;