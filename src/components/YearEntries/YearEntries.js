import React, { Component } from "react";

import MonthEntries from "../MonthEntries/MonthEntries";
import Currency from "../Currency/Currency";

import "./YearEntries.css";

class YearEntries extends Component {
  render() {
    let output;

    /* eslint-disable no-unused-vars */
    const {settings, year, entries} = this.props;

    let classes = [
      'YearEntries',
      'YearEntries-' + year
    ];
    /* eslint-enable no-unused-vars */

    let months = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    const now = new Date();

    const currentYear = now.getFullYear();
    const currentMonth = now.getMonth() + 1;

    let containers = [];

    for (const key in months) {
      const month = parseInt(key, 10) + 1;

      if (year === currentYear && month > currentMonth) {
        // Don't display future months
        break;
      }

      const name = months[key];
      const monthEntries = entries['entries'][month];

      if (settings.showEmpty || monthEntries['entries'].length) {
        containers.push(
          (
            <MonthEntries
              key={year + '_' + month}
              settings={settings}
              year={year}
              month={month}
              name={name}
              entries={monthEntries}
              onNewEntry={this.props.onNewEntry}
              onEntryChange={this.props.onEntryChange}
              onEntrySave={this.props.onEntrySave}
              onEntryDelete={this.props.onEntryDelete}
            />
          )
        );
      }
    }

    let penalties = null;

    if (entries.penalty) {
      penalties = (
        <dl class="penalty">
          <dt>
            {this.props.year} Total Penalties
          </dt>
          <dd>
            <Currency
              value={entries.penalty}
            />
          </dd>
        </dl>
      );
    }

    output = (
      <form className={classes.join(' ')}>
        <h2>
          {this.props.year}
        </h2>
        {penalties}
        {containers}
      </form>
    );

    return output;
  }
}

export default YearEntries;