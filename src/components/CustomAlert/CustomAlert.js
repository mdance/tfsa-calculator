import React, { Component } from "react";

import { Alert } from "react-bootstrap";

export default class CustomAlert extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleDismiss = this.handleDismiss.bind(this);
    this.handleShow = this.handleShow.bind(this);

    this.state = {
      show: true
    };
  }

  handleDismiss() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    if (this.state.show) {
      // TODO: Fix dismiss functionality
      const {children, ...rest} = this.props;

      return (
        <Alert {...rest} onDismiss={this.handleDismiss}>
          {children}
        </Alert>
      );
    }
  }
}