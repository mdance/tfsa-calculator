import React, { Component } from "react";
import "./DropzoneContainer.css";

import Dropzone from 'react-dropzone';

export default class DropzoneContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      accept: '',
      files: [],
      dropzoneActive: false,
      onFile: props.onFile
    };
  }

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  onDragEnter() {
    this.setState({
      dropzoneActive: true
    });
  }

  onDragLeave() {
    this.setState({
      dropzoneActive: false
    });
  }

  onDrop(files) {
    for (const key in files) {
      this.props.onFile(files[key]);
    }

    this.setState({
      dropzoneActive: false
    });
  }

  render() {
    // TODO: Change dropzone positioning to fixed
    // TODO: Improve dropzone styling
    // TODO: Improve description styling
    const { accept, dropzoneActive } = this.state;

    return (
        <Dropzone
          className="DropzoneContainer"
          disableClick
          accept={accept}
          onDragEnter={this.onDragEnter.bind(this)}
          onDrop={this.onDrop.bind(this)}
          onDragLeave={this.onDragLeave.bind(this)}
        >
          {
          dropzoneActive &&
          <div className="overlay">
            <p>
              {this.props.description}
            </p>
          </div> 
          }
          {this.props.children}
        </Dropzone>
    );
  }
}
