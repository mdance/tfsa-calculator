import React, { Component } from 'react'
import numeral from 'numeral'

class Currency extends Component {

  render() {
    const className = this.props.className
    const value = this.props.value
    const format = this.props.format
    const formated = (format)?numeral(value).format(format):numeral(value)

    if (value) {
      return (
        <span className={`${(className)?className:''}`}>
          {formated}
        </span>
      );

    } else {
      return <noscript />
    }
  }

}

Currency.defaultProps = {
  className: 'currency',
  format: '$0,0.00'
}

export default Currency;