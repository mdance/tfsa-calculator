// Core
import React, { Component, Fragment } from "react";

// Routing
import { Link, withRouter } from "react-router-dom";
import Routes from "./Routes";

// Services
import LocalStorageService from "./services/LocalStorageService";
import SettingsService from "./services/SettingsService";
import EntriesService from "./services/EntriesService";
import CalculationService from "./services/CalculationService";
import FileService from "./services/FileService";

// UI
import nprogress from 'react-nprogress';
import 'react-nprogress/nprogress.css';

import { Nav, Navbar, NavItem, MenuItem, Glyphicon, SplitButton } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import CustomAlert from "./components/CustomAlert/CustomAlert";

import DropzoneContainer from "./components/DropzoneContainer/DropzoneContainer";

// Styling
import "./App.css";

// TODO: Write unit tests
// TODO: Figure out why automation tests are failing on production build
// TODO: Implement github pages build
// TODO: Setup terraform
// TODO: Setup ansible containers
// TODO: Implement redux

class App extends Component {
  constructor(props) {
    super(props);

    SettingsService.setStorageService(LocalStorageService);
    EntriesService.setStorageService(LocalStorageService);

    let settings = SettingsService.get();
    
    let entries = EntriesService.get(settings);
    
    CalculationService.calculate(entries, settings);

    let methods = [
      'handleMessage',
      'handleDefaultSettings',
      'handleFile',
      'handleDefaultEntries',
      'handleSettingsChange',
      'handleNewEntry',
      'handleEntryChange',
      'handleEntrySave',
      'handleEntryDelete',
    ];

    for (const delta in methods) {
      this[methods[delta]] = this[methods[delta]].bind(this);
    }

    this.state = {
      messages: [],
      handlers: {
        handleMessage: this.handleMessage,
        handleDefaultSettings: this.handleDefaultSettings,
        handleFile: this.handleFile,
        handleDefaultEntries: this.handleDefaultEntries,
        handleSettingsChange: this.handleSettingsChange,
        handleNewEntry: this.handleNewEntry,
        handleEntryChange: this.handleEntryChange,
        handleEntrySave: this.handleEntrySave,
        handleEntryDelete: this.handleEntryDelete
      },
      services: {
        LocalStorageService: LocalStorageService,
        SettingsService: SettingsService,
        EntriesService: EntriesService,
        CalculationService: CalculationService,
        FileService: FileService
      },
      settings: settings,
      entries: entries,
    };
  }

  /**
   * Responsible for starting the progress bar display.
   */
  componentWillMount () {
    nprogress.start()
  }

  /**
   * Responsible for hiding the progress bar display.
   */
  componentDidMount () {
    nprogress.done()
  }

  /**
   * Responsible for building the year dropdown menu.
   */
  linkContainerYears() {
    // TODO: Convert this to a YearsDropdown component
    // TODO: Figure out why the links aren't working
    let years = this.state.settings.activeYears.slice(0);
    
    years.reverse();

    const output = (
      <SplitButton
        id="menu-entries"
        bsStyle="default"
        title="Entries"
        href="/entries"
      >
        {
          years.map(
            (key) => {
              const to = '/entries/' + key;

              return (
                <MenuItem key={key} href={to}>
                  {key}
                </MenuItem>
              );
            }
          )
        }
      </SplitButton>
    );

    return output;
  }

  /**
   * Responsible for handling file uploads.
   * 
   * @param {*} file 
   */
  handleFile(file) {
    const {settings} = this.state;

    const options = {
      file: file,
      success: (entries) => {
        let options = Object.assign({}, settings);
        
        options.entries = entries;

        entries = EntriesService.get(options);

        CalculationService.calculate(entries, options);

        this.setState(
          {
            entries: entries
          },
          () => {
            EntriesService.set(this.state.entries);

            this.props.history.push('/entries');
          }
        );

      },
      error: (error, file) => {
        // TODO: Fix and test global error boundaries and handler
        this.handleMessage(
          <CustomAlert 
            bsStyle="danger">
            An error has occurred processing your file upload.
          </CustomAlert>
        );
      },
      amountFormat: this.state.settings.amountFormat
    };

    FileService.handleFile(options);
  }

  /**
   * Responsible for setting the default settings.
   */
  handleDefaultSettings() {
    this.setState(
      (prevState, props) => {
        let settings = SettingsService.get(true);
        
        let entries = EntriesService.get(settings);

        let output = {
          settings: settings,
          entries: entries
        };

        return output;
      },
      () => {
        SettingsService.set(this.state.settings);
      }
    );
  }

  /**
   * Responsible for handling settings changes.
   */
  handleSettingsChange(input) {
    this.setState(
      (prevState, props) => {
        let output = {
          settings: input
        };

        return output;
      },
      () => {
        SettingsService.set(this.state.settings);
      }
    );
  }

  /**
   * Responsible for setting the default entries.
   */
  handleDefaultEntries() {
    this.setState(
      (prevState, props) => {
        let output = {};

        let options = Object.assign({}, prevState.settings);
        
        let entries = EntriesService.defaultEntries();

        options.entries = entries;

        entries = EntriesService.get(options);

        CalculationService.calculate(entries, options);

        output.entries = entries;

        return output;
      },
      () => {
        EntriesService.set(this.state.entries);
      }
    );
  }

  /**
   * Responsible for handling new entries.
   * 
   * @param {*} input 
   */
  handleNewEntry(change) {
    this.setState(
      (prevState, props) => {
        const {year, month, delta} = change;

        let output = {...prevState};

        EntriesService.newEntry(output.entries, year, month, delta);

        CalculationService.calculate(output.entries, prevState.settings);

        return output;
      },
      () => {
        EntriesService.set(this.state.entries);
      }
    );
  }

  /**
   * Responsible for handling entry changes.
   * 
   * @param {*} input 
   */
  handleEntryChange(change) {
    this.setState(
      (prevState, props) => {
        let output = {};

        output.entries = EntriesService.updateEntry(prevState.entries, change);

        CalculationService.calculate(output.entries);

        return output;
      }
    );
  }

  /**
   * Responsible for handling entry saves.
   * 
   * @param {*} input 
   */
  handleEntrySave(change) {
    this.setState(
      (prevState, props) => {
        let output = {};

        let entries = EntriesService.set(prevState.entries);

        output.entries = entries;

        return output;
      }
    );
  }

  /**
   * Responsible for handling entry deletions.
   * 
   * @param {*} input 
   */
  handleEntryDelete(change) {
    this.setState(
      (prevState, props) => {
        const {year, month, delta} = change;

        let output = {
          entries: prevState.entries
        };

        output['entries'][year]['entries'][month]['entries'].splice(delta, 1);

        CalculationService.calculate(output.entries);

        return output;
      },
      () => {
        EntriesService.set(this.state.entries);
      }
    );
  }

  /**
   * Responsible for handling new messages.
   * 
   * @param {*} input 
   */
  handleMessage(input) {
    this.setState(
      (prevState, props) => {
        let output = {...prevState};

        output.messages.push(input);

        return output;
      }
    );
  }

  /**
   * Responsible for rendering the application.
   */
  render() {
    const childProps = this.state;

    const {messages} = this.state;

    let messageContainer = '';

    if (messages.length) {
      messageContainer = (
        <div className="Messages">
          {messages}
        </div>
      );
    }

    // TODO: Move Navbar to CustomNavbar component
    return (
      <div className="Wrapper">
        <DropzoneContainer
          onFile={(file) => this.handleFile(file)}
          description="Drop a CSV file containing your Tax Free Savings Account entries..."
        >
          <div className="App container">
            <Navbar fluid collapseOnSelect>
              <Navbar.Header>
                <Navbar.Brand>
                  <Link to="/">Tax Free Savings Account Calculator</Link>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                <Nav pullRight>
                  <Fragment>
                    <LinkContainer to="/settings">
                      <NavItem>
                        <Glyphicon glyph="cog" />
                      </NavItem>
                    </LinkContainer>
                    {this.linkContainerYears()}
                  </Fragment>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
            {messageContainer}
            <Routes childProps={childProps} />
          </div>
        </DropzoneContainer>
      </div>
    );
  }
}

export default withRouter(App);