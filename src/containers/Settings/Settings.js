// Core
import React, { Component } from "react";

// UI
import {
  FormGroup,
  ControlLabel,
  FormControl,
  Table,
  Checkbox,
  Panel
} from "react-bootstrap";

import LoaderButton from "../../components/LoaderButton/LoaderButton";

import CustomAlert from "../../components/CustomAlert/CustomAlert";

// Styles
import "./Settings.css";

export default class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    }

    let methods = [
      'handleSubmit',
      'handleDefaultSettings',
      'handleDefaultEntries',
    ];

    for (const delta in methods) {
      this[methods[delta]] = this[methods[delta]].bind(this);
    }
  }

  /**
   * Responsible for handling the form submission.
   * 
   * @param {*} event 
   */
  handleSubmit(event) {
    event.preventDefault();

    let changed = false;
    let settings = {...this.props.settings};
    
    let activeYears = [];

    const keys = Object.keys(event.target);

    let newActiveYear = {
      checked: false,
      year: 0,
      limit: 0
    }

    for (const key in keys) {
      const element = event.target[key];

      if (element) {
        const id = element['id'];

        if (id) {
          if (id.startsWith('activeYears')) {
            if (element.checked) {
              const year = element.attributes['data-year'].value;

              if (year === 'new') {
                newActiveYear.checked = true;
              }
              else {
                activeYears.push(parseInt(element.attributes['data-year'].value, 10));
              }
            }
          }
          else if (id === 'year') {
            newActiveYear.year = parseInt(element.value, 10);
          }
          else if (id === 'limit') {
            newActiveYear.limit = parseInt(element.value, 10);
          }
          else if (id === 'showOpeningBalance') {
            if (settings.showOpeningBalance !== element.checked) {
              changed = true;

              settings.showOpeningBalance = element.checked;
            }
          }
          else if (id === 'showEmpty') {
            if (settings.showEmpty !== element.checked) {
              changed = true;

              settings.showEmpty = element.checked;
            }
          }
          else if (id === 'amountFormat') {
            if (settings.amountFormat !== element.value) {
              changed = true;

              settings.amountFormat = element.value;
            }
          }
        }
      }
    }

    const maxYear = Object.keys(settings.limits).sort().pop();

    if (newActiveYear.year > maxYear && newActiveYear.limit > 0) {
      changed = true;

      settings.limits[newActiveYear.year] = newActiveYear.limit;

      if (newActiveYear.checked) {
        activeYears.push(newActiveYear.year);
      }
    }

    if (settings.activeYears !== activeYears) {
      changed = true;

      settings.activeYears = activeYears;
    }

    if (changed) {
      this.props.handlers.handleSettingsChange(settings);
    }

    this.props.handlers.handleMessage(
      <CustomAlert 
        bsStyle="success">
        Your settings have been saved.
      </CustomAlert>
    );
  }

  /**
   * Responsible for handling the default settings form submission.
   * 
   * @param {*} event 
   */
  handleDefaultSettings(event) {
    event.preventDefault();

    this.props.handlers.handleDefaultSettings();

    this.props.handlers.handleMessage(
      <CustomAlert 
        bsStyle="success">
        Your settings have been reverted back to the defaults.
      </CustomAlert>
    );
  }

  /**
   * Responsible for handling the default entries form submission.
   * 
   * @param {*} event 
   */
  handleDefaultEntries(event) {
    event.preventDefault();

    this.props.handlers.handleDefaultEntries();

    this.props.handlers.handleMessage(
      <CustomAlert 
        bsStyle="success">
        Your entries have been reverted back to the defaults.
      </CustomAlert>
    );
  }

  renderForm() {
    const {settings} = this.props;
    const {activeYears, limits} = settings;

    let disabled = false;

    let rows = [];

    for (let year in limits) {
      year = parseInt(year, 10);
      
      const limit = limits[year];
      const id = "activeYears" + year + "";
      const name = "activeYears[" + year + "]";
      
      let checked = true;

      if (activeYears.indexOf(year) === -1) {
        checked = false;
      }

      // TODO: Add select all checkbox
      rows.push(
            <tr>
              <td>
                <Checkbox 
                  id={id}
                  name={name}
                  data-year={year}
                  defaultChecked={checked}
                />
              </td>
              <td className="right">
                {year}
              </td>
              <td className="right">
                {limit} 
              </td>
            </tr>
      );
    }

    rows.push(
            <tr>
              <td>
                <FormGroup bsSize="lg">
                  <Checkbox
                    id="activeYearsnew"
                    data-year="new"
                  /> 
                </FormGroup>
              </td>
              <td className="right">
                <FormControl 
                  type="text" 
                  id="year"
                  size="4"
                /> 
              </td>
              <td className="right">
                <FormControl 
                  type="text" 
                  id="limit"
                  size="4"
                /> 
              </td>
            </tr>
    );

    // TODO: Fix loader buttons
    return (
      <form onSubmit={this.handleSubmit}>
        <Table className="table-activeyears">
          <thead>
            <tr>
              <th>
                Active
              </th>
              <th className="right">
                Year
              </th>
              <th className="right">
                Contribution Limit
              </th>
            </tr>
          </thead>
          <tbody>
            {rows}
          </tbody>
        </Table>
        <Panel>
          <Panel.Heading>
            Other Settings
          </Panel.Heading>
          <Panel.Body>
            <FormGroup>
              <Checkbox
                id="showOpeningBalance"
                defaultChecked={settings.showOpeningBalance}
              >
                Show monthly opening balance
              </Checkbox>
            </FormGroup>
            <FormGroup>
              <Checkbox
                id="showEmpty"
                defaultChecked={settings.showEmpty}
              >
                Show empty months
              </Checkbox>
            </FormGroup>
            <FormGroup controlId="amountFormat">
              <ControlLabel>Amount Format</ControlLabel>
              <FormControl 
                componentClass="select" 
                placeholder="select"
                defaultValue={settings.amountFormat}
              >
                <option value="">Select</option>
                <option value="cents">Cents</option>
                <option value="dollars">Dollars</option>
              </FormControl>
            </FormGroup>
          </Panel.Body>
        </Panel>
        <LoaderButton
          className="btn-apply"
          block
          bsSize="large"
          disabled={disabled}
          type="submit"
          isLoading={this.state.isLoading}
          text="Apply"
          loadingText="Saving Settings..."
        />
        <LoaderButton
          className="btn-settings"
          block
          bsSize="large"
          type="submit"
          isLoading={this.state.isLoading}
          text="Default Settings"
          loadingText="Restoring default settings..."
          onClick={this.handleDefaultSettings}
        />
        <LoaderButton
          className="btn-entries"
          block
          bsSize="large"
          type="submit"
          isLoading={this.state.isLoading}
          text="Clear Entries"
          loadingText="Restoring default entries..."
          onClick={this.handleDefaultEntries}
        />
      </form>
    );
  }

  render() {
    return (
      <div className="Settings">
        {this.renderForm()}
      </div>
    );
  }
}
