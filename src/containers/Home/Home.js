import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Home.css";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    this.setState({ isLoading: false });
  }

  handleYearClick = event => {
    event.preventDefault();
    this.props.history.push(event.currentTarget.getAttribute("href"));
  }

  renderYears() {
    return (
      <div className="lander">
        <h1>TFSA Calculator</h1>
        <p>Drop your TFSA entries CSV file to calculate your TFSA interest penalties!</p>
        <div>
          <Link to="/settings" className="btn btn-success btn-lg">
            Settings
          </Link>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        {this.renderYears()}
      </div>
    );
  }
}
