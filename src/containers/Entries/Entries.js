import React, { Component } from "react";

import YearEntries from "../../components/YearEntries/YearEntries";

import "./Entries.css";

class Entries extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    const year = props.match.params.year;

    if (year) {
      this.state.years = [
        year
      ];
    }
    else {
      this.state.years = props.settings.activeYears;
    }
  }

  render() {
    // TODO: Add CSV export functionality
    let output = [];

    const {years} = this.state;

    const {settings, entries} = this.props;

    for (const key in years) {
      const year = years[key];

      try {
        let total = 0;

        for (const month in entries[year]['entries']) {
          total += entries[year]['entries'][month]['entries'].length;
        }

        if (settings.showEmpty || ( (settings.showOpeningBalance && total > 2) || (!settings.showOpeningBalance && total > 1))) {
          output.push(
            <YearEntries
              key={year}
              settings={settings}
              year={year}
              entries={entries[year]}
              onNewEntry={this.props.handlers.handleNewEntry}
              onEntryChange={this.props.handlers.handleEntryChange}
              onEntrySave={this.props.handlers.handleEntrySave}
              onEntryDelete={this.props.handlers.handleEntryDelete}
            />
          );
        }
      } catch (error) {}
    }

    return (
      <div className="Entries">
        {output}
      </div>
    );
  }
}

export default Entries;