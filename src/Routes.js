import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home/Home";
import Settings from "./containers/Settings/Settings";
import Entries from "./containers/Entries/Entries";
import NotFound from "./containers/NotFound/NotFound";
import CustomRoute from "./components/CustomRoute/CustomRoute";

export default ({ childProps }) =>
  <Switch>
    <CustomRoute path="/" exact component={Home} props={childProps} />
    <CustomRoute path="/settings" exact component={Settings} props={childProps} />
    <CustomRoute path="/entries/:year?" exact component={Entries} props={childProps} />
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;
