#!/bin/bash
if [ -z "${CI_REGISTRY_PASSWORD}" ]; then
  echo "Please set the CI_REGISTRY_PASSWORD environment variable before running this script."
  exit
fi

(
  cd ../.. &&
  gitlab-runner \
  exec \
  docker \
  build \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --env "CI_REGISTRY_USER=mdance" \
  --env "CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD}" \
  --env "CI_REGISTRY=registry.gitlab.com" \
  #--env "CI_COMMIT_REF_NAME=local"
)